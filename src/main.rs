use egg::{SymbolLang as S, *};
//use rug::ops::MulFrom;
use rug::Integer;
use rug::Rational;

#[derive(Debug)]
pub struct Pref;
impl<L: Language + std::fmt::Display> CostFunction<L> for Pref {
    type Cost = Rational;
    fn cost<C>(&mut self, enode: &L, mut costs: C) -> Self::Cost
    where
        C: FnMut(Id) -> Self::Cost,
    {
        // cost computation diverges with smaller and smaller numbers
        if enode.is_leaf() {
            return Rational::from_f32(1.0).unwrap();
        }

        let kids_cost = enode.fold(Rational::from_f32(0.0).unwrap(), |sum, id| sum + costs(id));
        let node_cost = Rational::from_f32(0.1).unwrap() * kids_cost;
        println!("cost of {} is {}", enode, node_cost);
        node_cost
    }
}

pub struct Pref2;
impl<L: Language + std::fmt::Display> CostFunction<L> for Pref2 {
    type Cost = u64;
    fn cost<C>(&mut self, enode: &L, mut _costs: C) -> Self::Cost
    where
        C: FnMut(Id) -> Self::Cost,
    {
        // cost computation conclused, but no term ever extracted
        let res = if enode.is_leaf() { 2 } else { 1 };

        println!("cost of {} is {}", enode, res);
        res
    }
}

pub struct Pref3;
impl<L: Language + std::fmt::Display> CostFunction<L> for Pref3 {
    type Cost = Integer;
    fn cost<C>(&mut self, enode: &L, mut costs: C) -> Self::Cost
    where
        C: FnMut(Id) -> Self::Cost,
    {
        // cost function that diverges with larger negative numbers
        if enode.is_leaf() {
            return Integer::from(0);
        }
        let kids_cost = enode.fold(Integer::from(0), |sum, id| sum + costs(id));
        let node_cost = kids_cost - 1;
        println!("cost of {} is {}", enode, node_cost);
        node_cost
    }
}

fn main() {
    let mut egraph = EGraph::<S, ()>::default();
    let x = egraph.add(S::leaf("x"));
    let fx = egraph.add(S::new("f", vec![x]));
    egraph.union(x, fx);

    let mut expr = RecExpr::default();
    let expr_x = expr.add(SymbolLang::leaf("x"));
    let expr_fx = expr.add(SymbolLang::new("f", vec![expr_x]));
    let _expr_eq = expr.add(SymbolLang::new("=", vec![expr_x, expr_fx]));
    let egraph_eq = egraph.add_expr(&expr);

    egraph.rebuild();

    // Extractors can take a user-defined cost function,
    // we'll use the egg-provided AstSize for now
    let extractor = Extractor::new(&egraph, Pref2);
    println!("Finished computing costs");
    let (best_cost, best_expr) = extractor.find_best(egraph_eq);

    // we found the best thing, which is just "a" in this case
    println!("best expr is: {}", &best_expr);
    println!("best cost is: {}", &best_cost);
    //assert_eq!(best_expr, "a".parse().unwrap());
    //assert_eq!(best_cost, 0);
}
